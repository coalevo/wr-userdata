/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.userdata.impl;

import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.model.UserdataPrivacy;
import net.coalevo.userdata.model.UserdataTokens;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

/**
 * This class implements a {@link Userdata} to JSON converter.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class Userdata2JSON {

  private JsonFactory m_JsonFactory = new JsonFactory();

  public void toJSON(Writer writer, Userdata ud, boolean filter) throws IOException {

    JsonGenerator w = m_JsonFactory.createJsonGenerator(writer);

    UserdataPrivacy priv = ud.getPrivacy();

    w.writeStartObject();
    writeStringField(w, UserdataTokens.ATTR_USERDATA_VERSION, UserdataTokens.VERSION);
    writeStringField(w, UserdataTokens.ATTR_USERDATA_UID, ud.getIdentifier());
    writeBooleanField(w, UserdataTokens.ATTR_CONFIRMED, ud.isConfirmed());
    writeBooleanField(w, UserdataTokens.ATTR_REVIEWED, ud.isReviewed());
    writeStringField(w, UserdataTokens.ELEMENT_NICKNAME, ud.getNickname());

    //write personal
    boolean start = false;
    String tmp = null;

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_PREFIX, ud.getPrefix(), priv.getAllowsPrefix());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_FIRSTNAME, ud.getFirstnames(), priv.getAllowsFirstnames());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_ADDITIONALNAME, ud.getAdditionalnames(), priv.getAllowsAdditionalnames());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_LASTNAME, ud.getLastnames(), priv.getAllowsLastnames());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_SUFFIX, ud.getSuffix(), priv.getAllowsSuffix());

    Date d = ud.getBirthdate();
    if (d != null) {
      tmp = DATE_FORMAT.format(ud.getBirthdate());

      start = writeComplexHideable(
          w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
          UserdataTokens.ELEMENT_BIRTHDATE, tmp, priv.getAllowsBirthdate());
    }
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_PERSONAL, start,
        UserdataTokens.ELEMENT_GENDER, ud.getGender(), priv.getAllowsGender());

    if (start) {
      w.writeEndObject();
    }

    //write address
    start = false;
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_STREET, ud.getStreet(), priv.getAllowsStreet());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_EXTENSION, ud.getExtension(), priv.getAllowsExtension());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_CITY, ud.getCity(), priv.getAllowsCity());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_COUNTRY, ud.getCountry(), priv.getAllowsCountry());
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_ADDRESS, start,
        UserdataTokens.ELEMENT_POSTALCODE, ud.getPostalCode(), priv.getAllowsPostalCode());
    if (start) {
      w.writeEndObject();
    }

    //communications
    start = false;
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_COMMUNICATIONS, start,
        UserdataTokens.ELEMENT_EMAIL, ud.getEmail(), priv.getAllowsEmail());

    tmp = ud.getLink();
    if (tmp != null && tmp.length() > 0) {
      start = conditionalStart(start, w, UserdataTokens.ELEMENT_COMMUNICATIONS);
      writeAttributeElement(w, UserdataTokens.ELEMENT_LINK, UserdataTokens.ATTR_TYPE, ud.getLinkType(), ud.getLink());
    }
    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_COMMUNICATIONS, start,
        UserdataTokens.ELEMENT_PHONE, ud.getPhone(), priv.getAllowsPhone());

    start = writeComplexHideable(
        w, filter, UserdataTokens.ELEMENT_COMMUNICATIONS, start,
        UserdataTokens.ELEMENT_MOBILE, ud.getMobile(), priv.getAllowsMobile());

    tmp = ud.getLanguages();
    if (tmp != null && tmp.length() > 0) {
      start = conditionalStart(start, w, UserdataTokens.ELEMENT_COMMUNICATIONS);
      writeStringField(w, UserdataTokens.ELEMENT_LANGUAGES, tmp);
    }
    if (start) {
      w.writeEndObject();
    }

    //photo
    tmp = ud.getPhoto();
    if (tmp != null && tmp.length() > 0) {
      writeAttributeElement(w, UserdataTokens.ELEMENT_PHOTO, UserdataTokens.ATTR_TYPE, ud.getPhotoType(), tmp);
    }
    //info
    tmp = ud.getInfo();
    if (tmp != null && tmp.length() > 0) {
      writeAttributeElement(w, UserdataTokens.ELEMENT_INFO, UserdataTokens.ATTR_FORMAT, ud.getInfoFormat(), tmp);
    }
    //flags
    Iterator iter = ud.getFlags();
    if (iter.hasNext()) {
      w.writeFieldName(UserdataTokens.ELEMENT_FLAGS);
      w.writeStartArray();
      for (Iterator iterator = ud.getFlags(); iterator.hasNext();) {
        tmp = (String) iterator.next();
        w.writeString(tmp);
      }
      w.writeEndArray();
    }

    //firstlogin
    d = ud.getFirstLoginDate();
    if (d != null) {
      writeStringField(w, UserdataTokens.ELEMENT_FIRSTLOGINDATE, DATE_FORMAT.format(d));
    }

    tmp = ud.getAnnotations();
    if (tmp != null && tmp.length() > 0) {
      writeStringField(w, UserdataTokens.ELEMENT_ANNOTATIONS, tmp);
    }


    w.writeEndObject();
    w.flush();
  }//writeTo

  private void writeStringField(JsonGenerator writer, String element, String content)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeString(content);
  }//writeStringField

  private void writeBooleanField(JsonGenerator writer, String element, boolean b)
      throws IOException {
    writer.writeFieldName(element);
    writer.writeBoolean(b);
  }//writeBooleanField


  private boolean writeComplexHideable(JsonGenerator writer, boolean filter, String pelem, boolean start, String element, String content, boolean allow)
      throws IOException {
    if (filter && !allow) {
      return start;
    }
    if (content != null && content.length() > 0) {
      start = conditionalStart(start, writer, pelem);
      writeSimpleHideableElement(writer, element, content, allow);
    }
    return start;
  }//writeComplexHideable

  private boolean conditionalStart(boolean b, JsonGenerator w, String element) throws IOException {
    if (!b) {
      w.writeFieldName(element);
      w.writeStartObject();
      return true;
    } else {
      return b;
    }
  }//conditionalStart

  private void writeSimpleHideableElement(JsonGenerator w, String element, String value, boolean allow)
      throws IOException {
    w.writeFieldName(element);
    w.writeStartObject();
    if (!allow) {
      w.writeFieldName(UserdataTokens.ATTR_HIDDEN);
      w.writeString("true");
    }
    w.writeFieldName("value");
    w.writeString(value);
    w.writeEndObject();
  }//writeSimpleHideableElement

  private void writeAttributeElement(JsonGenerator w, String element, String attname, String attvalue, String value)
      throws IOException {
    w.writeFieldName(element);
    w.writeStartObject();
    if (attvalue != null && attvalue.length() > 0) {
      w.writeFieldName(attname);
      w.writeString(attvalue);
    }
    w.writeFieldName("value");
    w.writeString(value);
    w.writeEndObject();
  }//writeAttributeElement

  private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

}//class Userdata2JSON
