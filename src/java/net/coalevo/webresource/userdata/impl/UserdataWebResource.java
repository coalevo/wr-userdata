/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.userdata.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.ServiceAgent;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.userdata.model.Userdata;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.userdata.service.UserdataXMLService;
import net.coalevo.webresource.model.BaseWebResource;
import net.coalevo.webresource.model.WebResource;
import net.coalevo.webresource.model.WebResourceException;
import net.coalevo.webresource.model.WebResourceRequest;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.NoSuchElementException;

/**
 * Provides a {@link WebResource} implementation that will provide
 * userdata.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class UserdataWebResource
    extends BaseWebResource
    implements ConfigurationUpdateHandler {

  private static final Marker c_LogMarker = MarkerFactory.getMarker(UserdataWebResource.class.getName());
  private ServiceMediator m_Services = Activator.getServices();
  private Userdata2JSON m_Userdata2Json = new Userdata2JSON();

  public UserdataWebResource() {
    super("userdata");
  }//constructor

  public void prepare() {
  }

  public void update(MetaTypeDictionary mtd) {

  }//update

  public void handle(WebResourceRequest resreq)
      throws WebResourceException {

    //1. prepare resources
    UserdataService ud = m_Services.getUserdataService(ServiceMediator.NO_WAIT);
    UserdataXMLService udxml = m_Services.getUserdataXMLService(ServiceMediator.NO_WAIT);
    ServiceAgent sag = resreq.getServiceAgent();

    //2. assert run
    if (ud == null || udxml == null || sag == null) {
      Activator.log().debug(c_LogMarker, "sag == " + ((sag == null) ? "NULL" : " NOT NULL"));
      resreq.failInternalError();
      return;
    }
    if (!resreq.hasSubResources()) {
      resreq.failBadRequest();
      return;
    }

    //3.handle subresources
    String[] subres = resreq.getSubResources();

    //4. means retrieval of userdata via agent identifier
    if (subres.length == 1) {
      handleRetrieve(resreq, ud, udxml);
      return;
    } else if (subres.length == 2) {
      if ("nick".equals(subres[0])) {
        handleNickResolve(resreq);
        return;
      } else if ("avatar".equals(subres[0])) {
        handleAvatarRetrieve(resreq, ud);
        return;
      } else if ("aid".equals(subres[0])) {
        handleAIDResolve(resreq);
        return;
      } else if ("search".equals(subres[0])) {
        handleSearch(resreq, ud);
        return;
      }
    }
    resreq.failBadRequest();
  }//handle

  private void handleRetrieve(WebResourceRequest resreq, UserdataService ud, UserdataXMLService udxml) {
    //asserted
    String aidin = resreq.getSubResources()[0];

    //Agent for authenticated
    
    AgentIdentifier aid = resreq.getAgentIdentifier(aidin);
    if (aid == null) {
      resreq.failBadRequest();
    }

    Userdata data = null;
    try {
      data = ud.getUserdata(resreq.getServiceAgent(), aid);
      if (data == null) {
        resreq.failNoContent();
        return;
      }
    } catch (NoSuchElementException nex) {
      resreq.failBadRequest();
      return;
    } catch (SecurityException ex) {
      resreq.failInternalError();
      return;
    }

    try {

      if (resreq.hasParameter(PARAM_FORMAT)
          && "JSON".equalsIgnoreCase(resreq.getParameter(PARAM_FORMAT))) {
        resreq.setJSONResponseType();
        m_Userdata2Json.toJSON(resreq.getOutputWriter(), data, true);
        return;
      } else {
        resreq.setXMLResponseType();
        PrintWriter pw = resreq.getOutputWriter();
        pw.println(XML_PREFIX);
        udxml.toXML(pw, data, true);
        return;
      }
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "UserdataResource::handleRetrieve()", ex);
    }
  }//handleRetrieve

  private void handleNickResolve(WebResourceRequest resreq) {
    //asserted
    String aidin = resreq.getSubResources()[1];
    try {
      resreq.quickResponse(resreq.resolveNickname(aidin));
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "UserdataResource::handleNickResolve()", ex);
    }
  }//handleNickResolve

  private void handleAIDResolve(WebResourceRequest resreq) {
    //asserted
    String nickin = resreq.getSubResources()[1];
    try {
      resreq.quickResponse(resreq.resolveAID(nickin).getIdentifier());
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "UserdataResource::handleAIDResolve()", ex);
    }
  }//handleAIDResolve

  private void handleAvatarRetrieve(WebResourceRequest resreq, UserdataService ud) {
    //asserted
    String aidin = resreq.getSubResources()[1];

    AgentIdentifier aid = resreq.getAgentIdentifier(aidin);
    if (aid == null) {
      resreq.failBadRequest();
    }

    Userdata data = null;
    try {
      data = ud.getUserdata(resreq.getServiceAgent(), aid);
      if (data == null || !data.hasPhoto()) {
        resreq.failNoContent();
        return;
      }
    } catch (NoSuchElementException nex) {
      resreq.failBadRequest();
      return;
    } catch (SecurityException ex) {
      resreq.failInternalError();
      return;
    }
    try {
      resreq.setImageResponseType(String.format("%s", data.getPhotoType()));
      resreq.getOutputStream().write(Base64.decodeBase64(data.getPhoto().getBytes("ascii")));
      return;
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "UserdataResource::handleAvatarRetrieve()", ex);
    }
  }//handleAvatarRetrieve


  private void handleSearch(WebResourceRequest resreq, UserdataService ud) {
    final String query = resreq.getSubResources()[1];
    AgentIdentifier[] ids = ud.searchPublicUserdata(
        resreq.getServiceAgent(), query
    );
    if (ids == null || ids.length == 0) {
      resreq.failNoContent();
      return;
    }
    try {
      resreq.setXMLResponseType();
      PrintWriter pw = resreq.getOutputWriter();
      pw.println(XML_PREFIX);
      pw.print(UD_LIST_START);
      pw.print(String.format(UD_QUERY_TEMPLATE, query));
      for (AgentIdentifier aid : ids) {
        String nick = ud.getNickname(resreq.getServiceAgent(), aid);
        pw.print(String.format(
            UD_AGENT_TEMPLATE,
            aid.getIdentifier(), nick,
            aid.getIdentifier())
        );
      }
      pw.print(UD_LIST_END);
    } catch (IOException ex) {
      Activator.log().error(c_LogMarker, "handle()", ex);
    }
  }//handleSearch

  private static final String PARAM_FORMAT = "format";

  private static final String UD_LIST_START = "<userdatalist xmlns=\"http://www.coalevo.net/schemas/userdata-list-v10\" version=\"1.0\">";
  private static final String UD_QUERY_TEMPLATE = "<query>%s</query>";
  private static final String UD_AGENT_TEMPLATE = " <agent id=\"%s\" nickname=\"%s\">%s</agent>";
  private static final String UD_LIST_END = "</userdatalist>";

}//class UserdataWebResource
