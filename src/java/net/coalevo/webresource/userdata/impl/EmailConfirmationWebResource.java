/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.webresource.userdata.impl;

import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.userdata.model.UserdataServiceException;
import net.coalevo.userdata.service.UserdataService;
import net.coalevo.webresource.model.BaseWebResource;
import net.coalevo.webresource.model.WebResource;
import net.coalevo.webresource.model.WebResourceException;
import net.coalevo.webresource.model.WebResourceRequest;
import net.coalevo.webresource.userdata.Configuration;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;


/**
 * Provides a {@link WebResource} implementation that supports
 * quick email confirmation.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class EmailConfirmationWebResource
    extends BaseWebResource
    implements ConfigurationUpdateHandler {

  private final Marker m_LogMarker = MarkerFactory.getMarker(EmailConfirmationWebResource.class.getName());

  private String m_SuccessPage = "/landing/email_confirmation_success.html";
  private String m_FailurePage = "/landing/email_confirmation_failure.html";

  public EmailConfirmationWebResource() {
    super("email");
  }//constructor

  public void update(MetaTypeDictionary mtd) {
    try {
      m_SuccessPage = mtd.getString(Configuration.EMAIL_LANDING_SUCCESS_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("error.configexception", "attribute", Configuration.EMAIL_LANDING_SUCCESS_KEY),
          ex
      );
    }
    try {
      m_FailurePage = mtd.getString(Configuration.EMAIL_LANDING_FAILURE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          Activator.getBundleMessages().get("error.configexception", "attribute", Configuration.EMAIL_LANDING_FAILURE_KEY),
          ex
      );
    }


  }//update

  public void handle(WebResourceRequest resreq)
      throws WebResourceException {

    //1. Prepare refs
    ServiceMediator sm = Activator.getServices();
    UserdataService ud = sm.getUserdataService(ServiceMediator.NO_WAIT);

    //2. assert run
    if (ud == null || resreq.getServiceAgent() == null) {
      resreq.failInternalError();
      return;
    }

    //3. retrieve request parts and verify
    String[] subres = resreq.getSubResources();
    if (subres.length != 3 || !"confirm".equals(subres[0])) {
      resreq.failBadRequest();
      return;
    }

    //4. Agent Identifier and Confirm ID
    AgentIdentifier aid = new AgentIdentifier(subres[1]);
    String cfid = subres[2];

    //5. Confirm
    boolean confirmed = false;
    try {
      confirmed = ud.confirmEmail(resreq.getServiceAgent(), aid, cfid);
    } catch (UserdataServiceException udex) {
      Activator.log().error(m_LogMarker, "handle()", udex);
    }

    if (confirmed) {
      resreq.redirect(m_SuccessPage);
    } else {
      resreq.redirect(m_FailurePage);
    }
  }//handle

}//class EmailConfirmationWebResource
